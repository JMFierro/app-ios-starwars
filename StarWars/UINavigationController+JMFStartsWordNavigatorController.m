//
//  UINavigationController+JMFStartsWordNavigatorController.m
//  StarWars
//
//  Created by José Manuel Fierro Conchouso on 04/02/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import "UINavigationController+JMFStartsWordNavigatorController.h"
#import "JMFCharacterViewController.h"
#import "NSData+JMFGetRecourses.h"


@implementation UINavigationController (JMFStarWars)



-(UINavigationController *) starWars:(NSString *) nameURL
                       fileNameSound:(NSString *) fileNameSound
              fileExtensionNameSound:(NSString *) fileExtenxionNameSound
                           fileImage:(NSString *) fileImage
                            firsName:(NSString *) firsName
                            lastName:(NSString *) lastName
                           aliasName:(NSString *) aliasName {
    
    
    
    // Modelo
    NSURL* recourseURL = [NSURL URLWithString: nameURL];
//    NSData* recourseSound = [NSData dataWithContentsOfURL:[[NSBundle mainBundle]
//                                                         URLForResource:fileNameSound
//                                                         withExtension:fileExtenxionNameSound]];
//    
    NSData *recourseSound = [[NSData alloc] init];
    recourseSound = [recourseSound dataWithRecourses:fileNameSound andExtension:fileExtenxionNameSound];
    
    UIImage* recourseImage = [UIImage imageNamed:fileImage];
    
    JMFCharacter* caharacter = [ JMFCharacter characterWithFirstName:firsName
                                                            lastName:lastName alias:aliasName
                                                            wikiPage:recourseURL
                                                               photo:recourseImage
                                                           soundData:recourseSound];
    
    // Controlador
    JMFCharacterViewController* characterVC = [[JMFCharacterViewController alloc ] initWithModel:caharacter];
    
    UINavigationController* characterNav = [[UINavigationController alloc] initWithRootViewController:characterVC];
    
   
    
    return characterNav;
   
    
}



-(UINavigationController *) starWars:(NSString *) name
                            firsName:(NSString *) firsName
                            lastName:(NSString *) lastName
                           aliasName:(NSString *) aliasName {
    
    
    UINavigationController* nav = [[UINavigationController alloc] init];
    nav = [nav starWars:[NSString stringWithFormat:@"http://en.wikipedia.org/wiki/%@",name]
                    fileNameSound:name
           fileExtensionNameSound: @"caf"
                        fileImage:[NSString stringWithFormat:@"%@.jpg",name]
                         firsName: firsName
                         lastName:lastName
                        aliasName:aliasName];
    
    
    return nav;
}

@end
