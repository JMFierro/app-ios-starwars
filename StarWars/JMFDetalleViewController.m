//
//  JMFDetalleViewController.m
//  StarWars
//
//  Created by José Manuel Fierro Conchouso on 07/02/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import "JMFDetalleViewController.h"

@interface JMFDetalleViewController ()

@end

@implementation JMFDetalleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
