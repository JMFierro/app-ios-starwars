//
//  main.m
//  StarWars
//
//  Created by Jorge Marcos Fernandez on 29/01/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JMFAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool
    {
        return UIApplicationMain( argc, argv, nil, NSStringFromClass( [ JMFAppDelegate class ] ) );
    }
}
