//
//  UINavigationController+JMFNavigationController.m
//  StarWars
//
//  Created by José Manuel Fierro Conchouso on 04/02/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import "UINavigationController+JMFNavigationController.h"
#import "JMFCharacterViewController.h"

@implementation UINavigationController (JMFNavigationController)

-(UINavigationController *) kk:(NSString *) nameURL
                       fileNameSound:(NSString *) fileNameSound
              fileExtensionNameSound:(NSString *) fileExtenxionNameSound
                           fileImage:(NSString *) fileImage
                            firsName:(NSString *) firsName
                            lastName:(NSString *) lastName
                           aliasName:(NSString *) aliasName {
    
    
    
    // Modelo
    NSURL* tarkinURL = [NSURL URLWithString: [NSString stringWithFormat:@" %@ parameter",nameURL]];
    NSData* tarkinSound = [NSData dataWithContentsOfURL:[[NSBundle mainBundle]
                                                         URLForResource:fileNameSound
                                                         withExtension:fileExtenxionNameSound]];
    UIImage* tarkinImage = [UIImage imageNamed:fileImage];
    
    JMFCharacter* caharacter = [ JMFCharacter characterWithFirstName:firsName
                                                            lastName:lastName alias:aliasName
                                                            wikiPage:tarkinURL
                                                               photo:tarkinImage
                                                           soundData:tarkinSound];
    
    // Controlador
    JMFCharacterViewController* characterVC = [[JMFCharacterViewController alloc ] initWithModel:caharacter];
    
    UINavigationController* characterNav = [[UINavigationController alloc] initWithRootViewController:characterVC];
    
    return characterNav;

    
}

@end
