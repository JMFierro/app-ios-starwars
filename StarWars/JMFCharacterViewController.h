//
//  JMFCharacterViewController.h
//  StarWars
//
//  Created by Jorge Marcos Fernandez on 30/01/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMFCharacter.h"
#import "CafPlayer.h"
#import "JMFPersonajesViewController.h"

@interface JMFCharacterViewController : UIViewController < UISplitViewControllerDelegate, JMFCrewViewControllerDelegate>

@property (nonatomic,strong)                JMFCharacter*   model;
@property (nonatomic,weak)      IBOutlet    UIImageView*    photoView;
@property (nonatomic,strong)                CafPlayer*      player;

-(id)initWithModel:(JMFCharacter*)model;

-(IBAction)plaSound:(id)sender;
-(IBAction)showWiki:(id)sender;
@end
