//
//  JMFCrew.m
//  StarWars
//
//  Created by José Manuel Fierro Conchouso on 06/02/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//


#import "JMFCrew.h"
#import "NSData+JMFGetRecourses.h"


@interface JMFCrew()

// Para que no se puedan modificar los arrays desde fuera de la clase
// "Strong"  es propietaria de estos array.
@property (nonatomic, strong) NSArray* imperialCharacters;
@property (nonatomic, strong) NSArray* rebelCharacters;

@end

@implementation JMFCrew

// Constructor de clase: método de clase, por eso se usa el nombre "crew" por convención.
// Los constructore de instancia deben llamarse init.
+(id)crew{
    
 
    
    NSURL* vaderURL = [NSURL URLWithString: @"http://en.wikipedia.org/wiki/Darth_Vader"];
    NSData *vaderSound = [[NSData alloc] init];
    vaderSound = [vaderSound dataWithRecourses:@"vader" andExtension:@"caf"];
    UIImage* vaderImage = [UIImage imageNamed:@"darthVader.jpg"];
    
    JMFCharacter* vader = [ JMFCharacter characterWithFirstName:@"Anakin"
                                                            lastName:@"SkyWalker"
                                                               alias:@"Darth Vader"
                                                            wikiPage:vaderURL
                                                               photo:vaderImage
                                                           soundData:vaderSound];
    
    NSURL* yodaURL = [NSURL URLWithString: @"http://en.wikipedia.org/wiki/yoda"];
    NSData *yodaSound = [[NSData alloc] init];
    yodaSound = [yodaSound dataWithRecourses:@"yoda" andExtension:@"caf"];
    UIImage* yodaImage = [UIImage imageNamed:@"yoda.jpg"];
    
    JMFCharacter* yoda = [ JMFCharacter characterWithFirstName:@"Minch"
                                                      lastName: @"Minch"
                                                         alias:@"Yoda"
                                                      wikiPage:yodaURL
                                                         photo:yodaImage
                                                     soundData:yodaSound];
    
    NSURL* c3poURL = [NSURL URLWithString: @"http://en.wikipedia.org/wiki/c3po"];
    NSData *c3poSound = [[NSData alloc] init];
    c3poSound = [c3poSound dataWithRecourses:@"c3po" andExtension:@"caf"];
    UIImage* c3poImage = [UIImage imageNamed:@"c3po.jpg"];
    
    JMFCharacter*  c3po= [ JMFCharacter characterWithFirstName:@"c3po"
                                                  lastName: @"robot"
                                                     alias:@"El intelectual"
                                                  wikiPage:c3poURL
                                                     photo:c3poImage
                                                 soundData:c3poSound];
    
    
    NSURL* tarkinURL = [NSURL URLWithString: @"http://en.wikipedia.org/wiki/tarkin"];
    NSData *tarkinSound = [[NSData alloc] init];
    tarkinSound = [tarkinSound dataWithRecourses:@"tarkin" andExtension:@"caf"];
    UIImage* tarkinImage = [UIImage imageNamed:@"tarkin.jpg"];
    
    JMFCharacter*  tarkin = [ JMFCharacter characterWithFirstName:@"tarkin"
                                                  lastName: @"malo2"
                                                     alias:@"jefe2"
                                                  wikiPage:tarkinURL
                                                     photo:tarkinImage
                                                 soundData:tarkinSound];
    
    NSURL* palpatineURL = [NSURL URLWithString: @"http://en.wikipedia.org/wiki/palpatine"];
    NSData *palpatineSound = [[NSData alloc] init];
    palpatineSound = [palpatineSound dataWithRecourses:@"palpatine" andExtension:@"caf"];
    UIImage* palpatineImage = [UIImage imageNamed:@"palpatine.jpg"];
    
    JMFCharacter*  palpatine= [ JMFCharacter characterWithFirstName:@"palpatine"
                                                  lastName: @"mostruo"
                                                     alias:@"El que dá miedo"
                                                  wikiPage:palpatineURL
                                                     photo:palpatineImage
                                                 soundData:palpatineSound];
    
    NSURL* chewbaccaURL = [NSURL URLWithString: @"http://en.wikipedia.org/wiki/chewbacca"];
    NSData *chewbaccaSound = [[NSData alloc] init];
    chewbaccaSound = [chewbaccaSound dataWithRecourses:@"chewbacca" andExtension:@"caf"];
    UIImage* chewbaccaImage = [UIImage imageNamed:@"chewbacca.jpg"];
    
    JMFCharacter*  chewbacca= [ JMFCharacter characterWithFirstName:@"Chewbacca"
                                                  lastName: @"MinchApellido"
                                                     alias:@"chewbaccaAlias"
                                                  wikiPage:chewbaccaURL
                                                     photo:chewbaccaImage
                                                 soundData:chewbaccaSound];
    
    
    JMFCrew *crew = [[JMFCrew alloc] init];
    [crew setImperialCharacters:@[vader, palpatine, tarkin]];
    [crew setRebelCharacters:@[yoda, c3po, chewbacca]];
    
    return crew;
    
    
}

//- (id)init
//{
//    self = [super initWithObjects:@"One", @"Two", nil];
////    self = [super initWithArray:array];
//    if (!self) return nil;
//    
//    return self;
//}

// Métodos de instancia que devuelven el numero de personajes.
-(int)imperialCount {
    return self.imperialCharacters.count;

}


-(int)rebelCount{
    
    return self.rebelCharacters.count;
    
}

// Métodos para saber cuantos elemntos hay en el array
-(JMFCharacter*)imperialCharacterAtIndex:(int)index{
    return [self.imperialCharacters objectAtIndex:index];
    
}


-(JMFCharacter*)rebelCharacterAtIndex:(int)index{
    return [self.rebelCharacters objectAtIndex:index];
    
}


@end
