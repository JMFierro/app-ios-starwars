//
//  JMFCharacterJuan.h
//  StarWars
//
//  Created by José Manuel Fierro Conchouso on 05/02/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JMFCharacterJuan : NSObject {
    @public
    int _year;
    NSString * _name;
}

//@property int year;
//// Solo genera Getters
@property (readonly,nonatomic, strong) NSString* name;
//@property (nonatomic, strong)NSString* name;

// Para darle el valor inicial
-(id)initWithName:(NSString*)name;

////Setters
-(void)setYear:(int)year;
//-(void)setName:(NSString*)name;


////Getters
-(int)year;
//-(NSString *)name;
//

@end
