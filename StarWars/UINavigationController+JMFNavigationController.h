//
//  UINavigationController+JMFNavigationController.h
//  StarWars
//
//  Created by José Manuel Fierro Conchouso on 04/02/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (JMFNavigationController)

-(UINavigationController *) kk:(NSString *)nameURL
                       fileNameSound:(NSString *) fileNameSound
              fileExtensionNameSound:(NSString *) fileExtenxionNameSound
                           fileImage:(NSString *) fileImage
                            firsName:(NSString *) firsName
                            lastName:(NSString *) lastName
                           aliasName:(NSString *) aliasName;

@end
