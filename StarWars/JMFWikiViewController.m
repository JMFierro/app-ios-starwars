//
//  JMFWikiViewController.m
//  StarWars
//
//  Created by Jorge Marcos Fernandez on 31/01/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import "JMFWikiViewController.h"
#import "JMFPersonajesViewController.h"

@interface JMFWikiViewController ()

@end

@implementation JMFWikiViewController

-(id)initWithModel:(JMFCharacter*) model
{
    if( self = [super initWithNibName:nil bundle:nil] )
    {
        _model = model;
        self.title = @"Wiki";
    }
    return self;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Suscipción al evento "characte
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(characterHasChanged:) name:CHARACTER_CHANGED_NOTIFICATION object:nil];
    
    
    
    //sincronicar la vista con el modelo
//    [self.browser setDelegate:self];
    self.browser.delegate = self;
    
    [self.browser loadRequest:[NSURLRequest requestWithURL:self.model.wikiPage]];
}


-(void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Desuscripción de notidicaciones
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self];
    
    
}

-(void)characterHasChanged:(NSNotification *)notification {
    
    //Recupera
    JMFCharacter *newCharacter = (JMFCharacter *)[notification.userInfo objectForKey:CHARACTER_KEY];
    
    //Actualiza
    self.model = newCharacter;
    self.title = self.model.alias;
    
    // Recarga la página
    [self.browser loadRequest:[NSURLRequest requestWithURL:self.model.wikiPage]];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

}


#pragma mark - UIWebViewDelegate
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.progressBar stopAnimating];
}

-(void)webViewDidStartLoad:(UIWebView *)webView {
    [self.progressBar startAnimating];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.progressBar stopAnimating];
}

-(BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if( navigationType == UIWebViewNavigationTypeLinkClicked || navigationType == UIWebViewNavigationTypeFormSubmitted )
    {
        return NO;
    }
    else return YES;
}

//-(void)characterChanged:(JMFCharacter *)character {
//    
//    self.model = character;
//    
//    [self.browser loadRequest:[NSURLRequest requestWithURL:self.model.wikiPage]];
//    
//}

@end
