//
//  JMFCharacterJuan.m
//  StarWars
//
//  Created by José Manuel Fierro Conchouso on 05/02/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import "JMFCharacterJuan.h"

@implementation JMFCharacterJuan


// Para darle el valor inicial
-(id)initWithName:(NSString*)name {
       self = [super init];
        if (self) {
            
            //initializations
            _name = name;
        }
        return self;
}

-(void)setYear:(int)year {
    // Da error
    //self.year = year;
    
    // Modo correcto
    _year = year;
    
}

//
//-(void)setName:(NSString)name {
//    
//}
-(int)year {
    return _year;
}
//
//-(NSString *)name {
//    return _name;
//}


@end
