//
//  JMFWikiViewController.h
//  StarWars
//
//  Created by Jorge Marcos Fernandez on 31/01/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMFCharacter.h"




//@interface JMFWikiViewController : UIViewController <UIWebViewDelegate, JMFCrewViewControllerDelegate>
@interface JMFWikiViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic,strong)             JMFCharacter*   model;
@property (nonatomic,weak)   IBOutlet    UIWebView*      browser;
@property (nonatomic,weak)   IBOutlet    UIActivityIndicatorView*      progressBar;

-(id) initWithModel:(JMFCharacter*) model;


@end
