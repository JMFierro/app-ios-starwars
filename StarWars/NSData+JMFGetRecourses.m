//
//  NSData+JMFGetRecourses.m
//  StarWars
//
//  Created by José Manuel Fierro Conchouso on 04/02/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import "NSData+JMFGetRecourses.h"

@implementation NSData (JMFGetRecourses)

-(id)dataWithRecourses:(NSString*)resourcesName andExtension:(NSString*)extension {
    
   
    
//    //Obtengo la URl del recurso
//    <#(NSURL *)#>resourceURL = [[NSBundle mainBlundle] URLForResource:resourcesName]
//          withExtension:extension];
//    
//    // Creo el NSData con el contenido del fichero que apunta a la URl
//    NSData dataWithContentsOfURL:resourcesName andExtension:extension;
    

    NSData* resourcesSound = [NSData dataWithContentsOfURL:[[NSBundle mainBundle]
                                                         URLForResource:resourcesName
                                                         withExtension:extension]];
    
    return resourcesSound;
    
}


@end
