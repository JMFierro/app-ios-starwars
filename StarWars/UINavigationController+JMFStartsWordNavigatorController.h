//
//  UINavigationController+JMFStartsWordNavigatorController.h
//  StarWars
//
//  Created by José Manuel Fierro Conchouso on 04/02/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (JMFStarWars)

-(UINavigationController *) starWars:(NSString *) nameURL
                       fileNameSound:(NSString *) fileNameSound
              fileExtensionNameSound:(NSString *) fileExtenxionNameSound
                           fileImage:(NSString *) fileImage
                            firsName:(NSString *) firsName
                            lastName:(NSString *) lastName
                           aliasName:(NSString *) aliasName ;


-(UINavigationController *) starWars:(NSString *)name
                            firsName:(NSString *) firsName
                            lastName:(NSString *) lastName
                           aliasName:(NSString *) aliasName;

@end
