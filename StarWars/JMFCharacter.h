//
//  JMFCharacter.h
//  StarWars
//
//  Created by Jorge Marcos Fernandez on 30/01/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JMFCharacter : NSObject

@property( nonatomic, strong ) NSString*    firstName;
@property( nonatomic, strong ) NSString*    lasttName;
@property( nonatomic, strong ) NSString*    alias;
@property( nonatomic, strong ) NSURL*       wikiPage;
@property( nonatomic, strong ) UIImage*     photo;
@property( nonatomic, strong ) NSData*      soundData;

//Class Methods
+(id)characterWithFirstName:(NSString*)firstName
                   lastName:(NSString*)lastName
                      alias:(NSString*)alias wikiPage:(NSURL*)wikiPage
                      photo:(UIImage*)image
                  soundData:(NSData*)soundData;

+(id)characterWithAlias:(NSString*)alias
               wikiPage:(NSURL*)wikiPage
                  photo:(UIImage*)image
              soundData:(NSData*)soundData;

//Designed Initializators
-(id)initWithFirstName:(NSString*)firstName
              lastName:(NSString*)lastName
                 alias:(NSString*)alias wikiPage:(NSURL*)wikiPage
                 photo:(UIImage*)image
             soundData:(NSData*)soundData;

-(id)initWithAlias:(NSString*)alias
          wikiPage:(NSURL*)wikiPage
             photo:(UIImage*)image
         soundData:(NSData*)soundData;

@end
