//
//  NSArray+JMFPersonajes.h
//  StarWars
//
//  Created by José Manuel Fierro Conchouso on 07/02/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NSArray_JMFPersonajes : NSArray

- (id)initWithArray:(NSArray *)array;
- (id)initWithObjects:(id *)objects count:(unsigned)count;
- (id)initWithObjects:(id)firstObj, ...;

@end
