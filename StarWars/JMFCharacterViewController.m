//
//  JMFCharacterViewController.m
//  StarWars
//
//  Created by Jorge Marcos Fernandez on 30/01/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import "JMFCharacterViewController.h"
#import "CafPlayer.h"
#import "JMFWikiViewController.h"

@interface JMFCharacterViewController ()

@end

@implementation JMFCharacterViewController


-(id)initWithModel:(JMFCharacter*)model
{
    if( self = [super initWithNibName:nil bundle:nil] )
    {
        _model = model;
        _player = [CafPlayer cafPlayer];
        self.title = model.alias;
        self.tabBarItem.image = model.photo; // mejor en le viewDidLoad ...

    }
    return self;
}


// Se llama una sola vez
-(void)viewDidLoad
{
    [super viewDidLoad];
    [[self photoView]setImage:[[self model]photo]];

    // Do any additional setup after loading the view from its nib.
//    self.tabBarItem.image = self.model.photo; // ...mejor aqui

}

-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// Se llama cada vez que se muestra
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //sincronizar modelo con vista
//    [[self photoView]setImage:[[self model]photo]]; 
//    self.photoView.image = self.model.photo;
}

#pragma mark - Actions
-(IBAction)plaSound:(id)sender
{
    [self.player playSoundData:self.model.soundData];
}

//-(IBAction)showWiki:(id)sender
-(IBAction)showWiki:(id)sender
{
    // Instancia
    JMFWikiViewController* wikiVC = [[JMFWikiViewController alloc]initWithModel:self.model];
    
    // Navegacion a la siguiente pantalla (necesario crear navigationController en el AppDelegate)
    [self.navigationController pushViewController:wikiVC animated:YES];
}


#pragma mark - UISplitViewController
-(void)splitViewController:(UISplitViewController *)svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)pc {
    // Posicion vertical: pone el boton
    self.navigationItem.leftBarButtonItem = barButtonItem;
}

-(void)splitViewController:(UISplitViewController *)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem {
    
    // Poscion horizontal: quita el boton
    self.navigationItem.leftBarButtonItem = nil;
}


#pragma mark - Crew delegate
-(void)personajesViewControlller:(JMFPersonajesViewController *)personajesViewController characterChanged:(JMFCharacter *)character {
    
    //Cambia el modelo
    self.model = character;
    
    //Sincroniza la vista
    self.photoView.image = self.model.photo;
    self.title = self.model.alias;
    
    
}

@end
