//
//  NSString+EjemploUTAD.h
//  StarWars
//
//  Created by José Manuel Fierro Conchouso on 04/02/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (EjemploUTAD)

-(NSString *) funnyCase;

@end
