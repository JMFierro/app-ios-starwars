//
//  JMFAppDelegate.m
//  StarWars
//
//  Created by Jorge Marcos Fernandez on 29/01/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import "JMFAppDelegate.h"
#import "JMFCharacter.h"
#import "JMFCharacterViewController.h"
#import "JMFWikiViewController.h"
#import "NSString+EjemploUTAD.h"
#import "UINavigationController+JMFStartsWordNavigatorController.h"
#import "JMFCharacterJuan.h"
#import "JMFCrew.h"
#import "JMFPersonajesViewController.h"


@implementation JMFAppDelegate

// didFinishLaunchingWithOptions -> 'Equivalente al onCreate.
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    

    
    // Parte izquierda
    JMFPersonajesViewController *listVC = [[JMFPersonajesViewController alloc] initWithStyle:UITableViewStyleGrouped];
    UINavigationController *listNav = [[UINavigationController alloc] initWithRootViewController:listVC];
    
    
    // Parte derecha
    JMFCrew *crew = [JMFCrew crew];
    JMFCharacterViewController *charVC = [[JMFCharacterViewController alloc]
                                          initWithModel:[crew rebelCharacterAtIndex:0]];
    UINavigationController *charNav = [[UINavigationController alloc] initWithRootViewController:charVC];
    
//    JMFWikiViewController *wikiVC = [[JMFWikiViewController alloc] initWithModel:[crew rebelCharacterAtIndex:0]];
//    UINavigationController *wikiNav = [[UINavigationController alloc] initWithRootViewController:wikiVC];
//    
//    listVC.delegate = wikiNav;
    
    // Crea SplitViewController
    UISplitViewController *splitVC = [[UISplitViewController alloc] init];
    [splitVC setViewControllers:@[listNav, charNav]];
    
    // Delegado de la lista
    listVC.delegate = charVC;
    
    // Delegado
    splitVC.delegate = charVC;
    
    // Pantalla a mostrar
    self.window.rootViewController = splitVC;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
