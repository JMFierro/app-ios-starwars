//
//  JMFAppDelegate.h
//  StarWars
//
//  Created by Jorge Marcos Fernandez on 29/01/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface JMFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
