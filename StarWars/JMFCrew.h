//
//  JMFCrew.h
//  StarWars
//
//  Created by José Manuel Fierro Conchouso on 06/02/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JMFCharacter.h"

@interface JMFCrew : NSObject




// Constructor de clase: método de clase, por eso se usa el nombre "crew" por convención.
// Los constructore de instancia deben llamarse init.
+(id)crew;

- (id)initWithArray:(NSArray *)array;
- (id)initWithObjects:(id *)objects count:(unsigned)count;
- (id)initWithObjects:(id)firstObj, ...;

// Métodos de instancia que devuelven el numero de personajes. 
-(int)imperialCount;
-(int)rebelCount;

// Métodos para saber cuantos elemntos hay en el array
-(JMFCharacter*)imperialCharacterAtIndex:(int)index;
-(JMFCharacter*)rebelCharacterAtIndex:(int)index;

@end
