//
//  JMFPersonajesViewController.h
//  StarWars
//
//  Created by José Manuel Fierro Conchouso on 06/02/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMFCrew.h"

#define CHARACTER_CHANGED_NOTIFICATION @"characterChanged"
#define CHARACTER_KEY @"character"

// Aviso de clase sin definir
@class JMFPersonajesViewController;

@protocol JMFCrewViewControllerDelegate

-(void) personajesViewControlller:(JMFPersonajesViewController*)personajesViewController
           characterChanged:(JMFCharacter*)character;

@end


@interface JMFPersonajesViewController : UITableViewController



// El objeto "delegate" implementa "JMFCrewViewControllerDelegate".
@property (weak, nonatomic) id<JMFCrewViewControllerDelegate>delegate;


@end
