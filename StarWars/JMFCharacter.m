//
//  JMFCharacter.m
//  StarWars
//
//  Created by Jorge Marcos Fernandez on 30/01/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import "JMFCharacter.h"

@implementation JMFCharacter

//Class Methods
+(id)characterWithFirstName:(NSString*)firstName
                   lastName:(NSString*)lastName
                      alias:(NSString*)alias
                   wikiPage:(NSURL*)wikiPage
                      photo:(UIImage*)image
                  soundData:(NSData*)soundData
{
    return [[self alloc] initWithFirstName:firstName lastName:lastName alias:alias wikiPage:wikiPage photo:image soundData:soundData];
}

+(id)characterWithAlias:(NSString*)alias
               wikiPage:(NSURL*)wikiPage
                  photo:(UIImage*)image
              soundData:(NSData*)soundData
{
    
    return [[self alloc] initWithFirstName:nil lastName:nil alias:alias wikiPage:wikiPage photo:image soundData:soundData];
}

//Designed Initializators
-(id)initWithFirstName:(NSString*)firstName
              lastName:(NSString*)lastName
                 alias:(NSString*)alias
              wikiPage:(NSURL*)wikiPage
                 photo:(UIImage*)image
             soundData:(NSData*)soundData
{
    if( self = [super init ] )
    {
        _alias = alias;
        _firstName = firstName;
        _lasttName = lastName;
        _wikiPage = wikiPage;
        _photo = image;
        _soundData = soundData;
    }
    return self;
}

-(id)initWithAlias:(NSString*)alias
          wikiPage:(NSURL*)wikiPage
             photo:(UIImage*)image
         soundData:(NSData*)soundData
{
    return [self initWithFirstName:nil
                          lastName:nil
                             alias:alias
                          wikiPage:wikiPage
                             photo:image
                         soundData:soundData];
}

@end
