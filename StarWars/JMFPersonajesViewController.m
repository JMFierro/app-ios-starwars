//
//  JMFPersonajesViewController.m (->JMFCrewViewController)
//  StarWars
//
//  Created by José Manuel Fierro Conchouso on 06/02/14.
//  Copyright (c) 2014 Jorge Marcos Fernandez. All rights reserved.
//

#import "JMFPersonajesViewController.h"
#import "JMFCrew.h"
#import "JMFCharacterViewController.h"

#define IMPERIAL_SECTION 0
#define REBEL_SECTION 1




/* Alternativa al ´define´
 
static const int kImperialSection = 0;
static const int kRebelSection = 0;
*/

@interface JMFPersonajesViewController ()

//@property (nonatomic,strong) (JMFCrew) crew;
@property (nonatomic, strong) JMFCrew *crew;

@end

@implementation JMFPersonajesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        self.crew = [JMFCrew crew];
        self.title = @"StarWars";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
//    JMFCrew *crew = [JMFCrew crew];
    
    if (section == IMPERIAL_SECTION) {
        return [self.crew imperialCount];
//        return crew.imperialCharacters.count;
    }
    else {
//        return crew.rebelCharacters.count;
        return [self.crew rebelCount];

    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
//    JMFCrew *crew = [JMFCrew crew];
    
    NSString *firsName = [[NSString alloc] init];
    NSString *aliasName = [[NSString alloc] init];
    UIImage *image = [[UIImage alloc] init];


    if (indexPath.section == IMPERIAL_SECTION) {
        

        JMFCharacter *imperialCharacters = [[JMFCrew crew] imperialCharacterAtIndex:indexPath.row];
        
        firsName = imperialCharacters .firstName;
        aliasName = imperialCharacters .alias;
        image = imperialCharacters .photo;

    }
    
    else {
        JMFCharacter *rebelCharacters = [[JMFCrew crew] rebelCharacterAtIndex:indexPath.row];

        firsName = rebelCharacters.firstName;
        aliasName = rebelCharacters.alias;
        image = rebelCharacters.photo;

    }


    cell.textLabel.text = firsName;
    cell.detailTextLabel.text = aliasName;
    cell.imageView.image = image;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (section == IMPERIAL_SECTION)
        return @"Personajes Imperiales";
    else
        return @"Personajes Rebeldes";
    
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath

{
    JMFCharacter *currentCharacter = [self characterAtIndexPath:indexPath];
    
    
    // Informa al delegado del personaje al que se ha cambiado.
//    [self.delegate characterChanged:currentCharacter];
    [self.delegate personajesViewControlller:self characterChanged:currentCharacter];
    
    // Accede a la instancia del Notificador
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    
    //Crea argumentos
//    NSDictionary *extraInfo = [NSDictionary dictionary];
    NSMutableDictionary *extraInfo = [[NSMutableDictionary alloc] init];
    
    // Le pasa el nuevo personaje
    [extraInfo setValue:currentCharacter forKey:CHARACTER_KEY];
    
    // Crea notificación
    NSNotification *note = [NSNotification notificationWithName:CHARACTER_CHANGED_NOTIFICATION                                                         object:self
                                                       userInfo:extraInfo];
    
    // Envio de la notificación
    [center postNotification:note];
    
    
    
//    JMFCharacterViewController *charVC = [[JMFCharacterViewController alloc] initWithModel:currentCharacter];
//    
//    [self.navigationController pushViewController:charVC animated:YES];
    
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
}

-(JMFCharacter*)characterAtIndexPath:(NSIndexPath*)indexPath {

    JMFCharacter *currentCharacter = nil;
    if (indexPath.section == IMPERIAL_SECTION)
        currentCharacter = [[JMFCrew crew] imperialCharacterAtIndex:indexPath.row];
    else
        currentCharacter = [[JMFCrew crew] rebelCharacterAtIndex:indexPath.row];
    
    return currentCharacter;
}

@end
